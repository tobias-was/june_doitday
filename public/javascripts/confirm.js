const sessionID = $.urlParam('sessionid');

const listItem = function (name, count, price) {
    return $(`<li class="order-list-item">
        <h2 class="font-bold item-title">${name}</h2>
        <ul class="order-sublist">
            <li class="order-sublist-item">
           <button class="button-sm button-blue item-type font-s text-center">QUANTITY</button><!-- 
           --><input class="item-quantity font-s text-center font-bold" type="number" value="${count}"><!--
           --><p class="item-price">£${price} x ${count} = £${price*count}</p>
          </li>
        </ul>
    </li>`);
}

const uniqueAndCount = function (products) {
    const count = {};
    return products.filter(product => {
        const { productId } = product;
        if (count.hasOwnProperty(productId)) {
            count[productId] = count[productId] + 1;
            return false;
        }
        count[productId] = 1;
        return true;
    }).map(product => {
        const { productId } = product;
        return Object.assign({}, { count: count[productId] }, product);
    });
}

$.ajax({
    url: `${settings.DOMINOS_API_ROOT}/api/facebookpoc/getBasketByGuid?guid=${sessionID}`,
    type: 'GET',
    crossdomain: 'true',
    dataType: 'json',	
    success: function(response) {
        const productIds = response.items.map(item => item.productId);
        $.get(`https://wasassets.s3.amazonaws.com/_beth/Misc/menu.json`, function (res, err) {
            const items = productIds.map(productId => res[productId]);
            const listItems = uniqueAndCount(items).map(product => listItem(product.name, product.count, product.price));
            const total = uniqueAndCount(items).reduce((currentTotal, product) => {
                return currentTotal + Number(product.price) * product.count;
            }, 0);
            $('.total').text(`£${total}`);
            $('.order-list').append(listItems);
        });
    },
    error: function(response) {
    	console.log("error");
    }
});
