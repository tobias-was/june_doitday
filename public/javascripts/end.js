$('a.share').click(function (e) {
  
  e.preventDefault();

  const messageToShare = {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'generic',
        elements: [{
          title: 'Domino\'s order (completed)',
          image_url: 'https://www.brandsoftheworld.com/sites/default/files/styles/logo-thumbnail/public/052013/dominos_logo.png?itok=rfMA20SQ',
          subtitle: 'Your dominos order',
          default_action: {
            type: 'web_url',
            messenger_extensions: true,
            url: `${settings.HTTPS_BASE_URL}/menu?sessionid=` + $.urlParam('sessionid')
          },
        buttons: [{
          type: 'web_url',
          messenger_extensions: true,
          url: `${settings.HTTPS_BASE_URL}/menu?sessionid=` + $.urlParam('sessionid'),
          title: 'Pizza!'
        }]
      }]
      }
    }
  };

  const onSuccess = function (response) {
    if (response.is_sent) {
      MessengerExtensions.requestCloseBrowser();
    } else {
      const keys = Object.keys(response).join(', ');
      $(".debugging").text(keys);
    }
  };

  const onError = function (errorCode, errorMessage) {
    console.log(errorCode, errorMessage);
    // $(".debugging").text(errorCode + ', ' + errorMessage);
  }

  MessengerExtensions.beginShareFlow(onSuccess, onError, messageToShare, 'current_thread');

  return false;

});