var productID = $.urlParam('productId');
var sessionID = $.urlParam('sessionid');
var name = decodeURIComponent($.urlParam('name'));
var description = decodeURIComponent($.urlParam('description'));
var imageUrl = decodeURIComponent($.urlParam('imageUrl'));

$('.product-name').text(name);
$('.product-description').text(description);
$('.product-img').attr('src', imageUrl);

$('.addItem').click(function (e) {

	e.preventDefault();

	$.ajax({
		url: `${settings.DOMINOS_API_ROOT}/api/facebookpoc/addBasketItem`,
		type: 'POST',
		crossdomain: 'true',
		dataType: 'json',
		data: {
		  item: {
		    productId: productID,
		    productSku: 0,
		    itemCount: 1
		  },
		  sessionGuid: sessionID,
		  fbUserId: '1234'
		},
		success: function (response) {
			window.location.replace(`/menu?sessionid=${sessionID}`);
		},
		error: function (error) {
			console.log(error);
		}
	});
	
	return false;

});