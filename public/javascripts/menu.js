const panel = function (product) {
  const { productId, imageUrl, name } = product;
  const queryParams = ['productId', 'imageUrl', 'name', 'description'];
  const queryString = queryParams.map(param => {
    return `${encodeURIComponent(param)}=${encodeURIComponent(product[param])}`;
  }).join('&');
  return $(`<a href="/submenu?sessionid=${$.urlParam('sessionid')}&${queryString}">
              <div class="carousel-panel">
                  <img src="${imageUrl}" />
                  <p class="carousel-copy font-s text-center">${name}</p>
              </div>
          </a>`);
};

$.ajax({
  url: `${settings.DOMINOS_API_ROOT}/api/facebookpoc/getmenu`,
  type: 'GET',
  crossdomain: 'true',
  dataType: 'json',
  data: {
    uid: '1234',
    postcode: $('#postcode').val(),
  },
  success: function (response) {
    response.sections.forEach(section => {
      const productName = section.name.toLowerCase();
      const products = section.products.map(product => panel(product));
      const $carousel = $('.' + productName + '.carousel .carousel-inner');
      const width = Math.ceil(products.length * 140).toString() + 'px';
      $carousel.css('width', width);
      $carousel.append(products);
    });
  },
  error: function (error) {
      console.log(error);
  }
});

$('a.share').click(function (e) {
  
  e.preventDefault();

  const messageToShare = {
    attachment: {
      type: 'template',
      payload: {
        template_type: 'generic',
        elements: [{
          title: 'Add to the Domino\'s order',
          image_url: 'https://www.brandsoftheworld.com/sites/default/files/styles/logo-thumbnail/public/052013/dominos_logo.png?itok=rfMA20SQ',
          subtitle: 'Your dominos order',
          default_action: {
            type: 'web_url',
            messenger_extensions: true,
            url: `${settings.HTTPS_BASE_URL}/menu?sessionid=` + $.urlParam('sessionid')
          },
        buttons: [{
          type: 'web_url',
          messenger_extensions: true,
          url: `${settings.HTTPS_BASE_URL}/menu?sessionid=` + $.urlParam('sessionid'),
          title: 'Pizza!'
        }]
      }]
      }
    }
  };

  const onSuccess = function (response) {
    if (response.is_sent) {
      MessengerExtensions.requestCloseBrowser();
    } else {
      
    }
  };

  const onError = function (errorCode, errorMessage) {
    console.log(errorCode, errorMessage);
  }

  MessengerExtensions.beginShareFlow(onSuccess, onError, messageToShare, 'current_thread');

  return false;

});