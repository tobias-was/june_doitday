const sessionID = $.urlParam('sessionid');

$.ajax({
  url: `${settings.DOMINOS_API_ROOT}/api/facebookpoc/getBasketByGuid?guid=${sessionID}`,
  type: 'GET',
  crossdomain: 'true',
  dataType: 'json',	
  success: function (response) {
    console.log(response);
  },
  error: function (response) {
    console.log("error");
  }
});
