$.urlParam = function (name) {
  const results = new RegExp(`[\?&]${name}=([^&#]*)`).exec(window.location.href);
  return results[1] || 0;
}
