const express = require('express');
const router = express.Router();
const menu = require('./data/menu');
const request = require('request');
const config = require('../config');

// Do a bit of data transformation to make it faster/easier to find products by their id
// (i.e. object where products are indexed by their id)
const products = [].concat.apply([], menu.sections.map(section => section.products)).reduce((obj, product) => {
  obj[product.productId.toString()] = product;
  return obj;
}, {});

router.get('/webhook', function(req, res, next) {
  if (req.query['hub.verify_token'] === 'testtoken') {
    console.log(req);
    res.send(req.query['hub.challenge']);
  } else {
    res.send('Error, wrong token');
  }
});

/* GET home page. */
router.get('/', function(req, res, next) {
  const { sessionid } = req.query;
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('index', { title: 'Dominos group order', page: 'start', sessionid, config: JSON.stringify(settings) });
});

router.get('/menu', function(req, res, next) {
  const { sessionid } = req.query;
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('menu', { title: 'Dominos group order', page: 'menu', sessionid, config: JSON.stringify(settings) });
});

router.get('/submenu', function(req, res, next) {
  const { sessionid } = req.query;
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('submenu', { title: 'Dominos group order', page: 'submenu', sessionid, config: JSON.stringify(settings) });
});

router.get('/confirm-order', function(req, res, next) {
  const { sessionid } = req.query;
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('confirm-order', { title: 'Dominos group order', page: 'confirm', sessionid, config: JSON.stringify(settings) });
});

router.get('/address', function(req, res, next) {
  const { sessionid } = req.query;
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('address', { title: 'Dominos group order', page: 'address', sessionid, config: JSON.stringify(settings) });
});

router.get('/payment-details', function(req, res, next) {
  const { sessionid } = req.query;
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('payment-details', { title: 'Dominos group order', page: 'payment', sessionid, config: JSON.stringify(settings) });
});

router.get('/end', function(req, res, next) {
  const { sessionid } = req.query;
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('end', { title: 'Dominos group order', page: 'end', sessionid, config: JSON.stringify(settings) });
});

router.get('/privacy', function(req, res, next) {
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('privacy', { title: 'Dominos group order', page: 'privacy', config: JSON.stringify(settings) });
});

router.get('/contact', function(req, res, next) {
  const { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL } = config;
  const settings = { DOMINOS_API_ROOT, HTTPS_BASE_URL, PRODUCTS_URL };
  res.render('contact', { title: 'Dominos group order', page: 'contact', config: JSON.stringify(settings) });
});

router.get('/getmenu/', function(req, res, next) {
  const { productId } = req.query;
  if (!!productId) {
    const selectedProducts = productId.map(id => products[id]);
    return res.json(selectedProducts);
  }  
  res.json(products);
});

router.get('/getmenu/:productId', function(req, res, next) {
  const { productId } = req.params;
  if (productId in products) {
    return res.json(products[productId]);  
  }
  res.json({ success : false, msg : `No product with id ${productId}.` });
});

module.exports = router;
